package com.maersk.knapsack.repository;

import com.maersk.knapsack.jpa.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query(value = "Select * from knapsack.task as tsk where tsk.task=?1", nativeQuery= true)
    Optional<Task> getTaskByTask(String task);
}
