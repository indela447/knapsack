package com.maersk.knapsack.repository;

import com.maersk.knapsack.jpa.Problem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProblemRepository extends JpaRepository<Problem, Long>
{
}
