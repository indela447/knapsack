package com.maersk.knapsack.repository;

import com.maersk.knapsack.jpa.Timestamps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimestampsRepository extends JpaRepository<Timestamps, Long>
{
}
