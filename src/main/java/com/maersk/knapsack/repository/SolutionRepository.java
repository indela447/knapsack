package com.maersk.knapsack.repository;

import com.maersk.knapsack.jpa.Solution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolutionRepository extends JpaRepository<Solution, Long>
{
}
