package com.maersk.knapsack.jpa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.GenerationType;

import java.io.Serializable;

@Entity
@Table(indexes = {@Index(name = "unique_index_task",  columnList="task", unique = true)})
@JsonIgnoreProperties({"id"})
public class Task implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private String task;
    private String status;
    @OneToOne
    private Timestamps timestamps;
    @OneToOne
    private Problem problem;
    @OneToOne
    private Solution solution;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public String getTask () {
        return task;
    }

    public void setTask (String task) {
        this.task = task;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Timestamps getTimestamps () {
        return timestamps;
    }

    public void setTimestamps (Timestamps timestamps) {
        this.timestamps = timestamps;
    }

    public Problem getProblem () {
        return problem;
    }

    public void setProblem (Problem problem) {
        this.problem = problem;
    }

    public Solution getSolution () {
        return solution;
    }

    public void setSolution (Solution solution) {
        this.solution = solution;
    }
}
