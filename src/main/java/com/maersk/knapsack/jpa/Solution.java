package com.maersk.knapsack.jpa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.ElementCollection;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.List;

@Entity
@JsonIgnoreProperties({ "id"})
public class Solution {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ElementCollection
    private List<Long> packed_items;
    private Long total_value;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public List<Long> getPacked_items () {
        return packed_items;
    }

    public void setPacked_items (List<Long> packed_items) {
        this.packed_items = packed_items;
    }

    public Long getTotal_value () {
        return total_value;
    }

    public void setTotal_value (Long total_value) {
        this.total_value = total_value;
    }
}

