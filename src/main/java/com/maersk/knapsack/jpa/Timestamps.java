package com.maersk.knapsack.jpa;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.sql.Timestamp;

@Entity
@JsonIgnoreProperties({ "id"})
public class Timestamps {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Timestamp submitted;
    private Timestamp started;
    private Timestamp completed;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public Timestamp getSubmitted () {
        return submitted;
    }

    public void setSubmitted (Timestamp submitted) {
        this.submitted = submitted;
    }

    public Timestamp getStarted () {
        return started;
    }

    public void setStarted (Timestamp started) {
        this.started = started;
    }

    public Timestamp getCompleted () {
        return completed;
    }

    public void setCompleted (Timestamp completed) {
        this.completed = completed;
    }
}
