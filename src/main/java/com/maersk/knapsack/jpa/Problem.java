package com.maersk.knapsack.jpa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.ElementCollection;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.List;

@Entity
@JsonIgnoreProperties({ "id"})
public class Problem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int capacity;
    @ElementCollection
    private List<Integer> weights;
    @ElementCollection
    private List<Integer> values2;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public int getCapacity () {
        return capacity;
    }

    public void setCapacity (int capacity) {
        this.capacity = capacity;
    }

    public List<Integer> getWeights () {
        return weights;
    }

    public void setWeights (List<Integer> weights) {
        this.weights = weights;
    }

    public List<Integer> getValues () {
        return values2;
    }

    public void setValues (List<Integer> values) {
        this.values2 = values;
    }
}
