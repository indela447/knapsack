package com.maersk.knapsack.service;

import com.maersk.knapsack.jpa.*;
import com.maersk.knapsack.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class TimestampsService
{

    @Autowired TimestampsRepository timestampsRepository;

    /**
     * Adding ot updating the Timestamps
     * @param stamps
     * @return
     */
    public Timestamps addTimeStamps (Timestamps stamps) {
        return timestampsRepository.save(stamps);
    }
}
