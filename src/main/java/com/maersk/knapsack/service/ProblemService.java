package com.maersk.knapsack.service;

import com.maersk.knapsack.jpa.*;
import com.maersk.knapsack.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
public class ProblemService
{

    @Autowired ProblemRepository problemRepository;

    /**
     * Adding or update the Problem
     * @param problem
     * @return
     */
    public Problem addProblem(Problem problem){
        return problemRepository.save(problem);
    }

    public Optional<Problem> getProblem(Long id){
        return problemRepository.findById(id);
    }

}
