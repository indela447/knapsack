package com.maersk.knapsack.service;

import com.maersk.knapsack.jpa.*;
import com.maersk.knapsack.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class SolutionService
{

    @Autowired SolutionRepository solutionRepository;

    /**
     * Adding or update the solution
     * @param solution
     * @return
     */
    public Solution addSolution (Solution solution) {
        return  solutionRepository.save(solution);
    }
}
