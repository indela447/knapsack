package com.maersk.knapsack.service;

import com.maersk.knapsack.jpa.*;
import com.maersk.knapsack.repository.*;
import com.maersk.knapsack.rest.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.scheduling.annotation.*;
import org.springframework.stereotype.*;

import java.sql.*;
import java.util.*;

@Service
public class TaskService {


    @Autowired TaskRepository taskRepository;
    @Autowired TimestampsService timestampsService;
    @Autowired SolutionService solutionService;

    /**
     * Adding or update Task
     * @param task
     * @return
     */
    public Task addTask (Task task) {
        return taskRepository.save(task);
    }

    /**
     * Get the Task by Id
     * @param id
     * @return
     */
    public Optional<Task> getTask (long id) {
        return taskRepository.findById(id);
    }

    /**
     * Get the Task by task Id
     * @param task
     * @return
     */
    public Optional<Task> getTaskByTask (String task) {
        return taskRepository.getTaskByTask(task);
    }

    /**
     * We are running time taking process in Asunc mode
     * Before starting algorithm, task status changing to started
     * After finish running the algorithm, task status changed to completed
     * @param task
     * @throws InterruptedException
     */
    @Async("asyncExecutor")
    public void processKnapsack(Task task) throws InterruptedException {
        Thread.sleep(10* 1000); //waiting 10 sec
        Timestamps timestamps = task.getTimestamps();
        timestamps.setStarted(new Timestamp(System.currentTimeMillis()));
        timestampsService.addTimeStamps(timestamps);
        task.setStatus(KnapsackController.STARTED);
        addTask(task);
        Thread.sleep(10* 1000); // waiting 10 sec
        solve(task.getProblem(), task);
        timestamps = task.getTimestamps();
        timestamps.setCompleted(new Timestamp(System.currentTimeMillis()));
        timestampsService.addTimeStamps(timestamps);
        task.setStatus(KnapsackController.COMPLETED);
        addTask(task);
    }

    /**
     * Ruuning Knapsack algorithm to update capacity and items in Solution
     * @param problem
     * @param task
     */
    private void solve(Problem problem, Task task) {

        int[] values = new int[problem.getValues().size()];
        int[] weight = new int[problem.getWeights().size()];
        int capacity = problem.getCapacity();

        for( int i = 0; i< problem.getValues().size(); i++){
            values[i] = problem.getValues().get(i);
        }
        for( int i = 0; i< problem.getWeights().size(); i++){
            weight[i] = problem.getWeights().get(i);
        }
        knapsackDyProg(weight,values,capacity, values.length ,task);
    }

    /**
     * Running dynamic knapsack solution
     * @param W
     * @param V
     * @param M
     * @param n
     * @param task
     */
    public void knapsackDyProg(int W[], int V[], int M, int n, Task task) {
        int B[][] = new int[n + 1][M + 1];

        for (int i=0; i<=n; i++)
            for (int j=0; j<=M; j++) {
                B[i][j] = 0;
            }
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= M; j++) {
                B[i][j] = B[i - 1][j];
                if ((j >= W[i-1]) && (B[i][j] < B[i - 1][j - W[i - 1]] + V[i - 1])) {
                    B[i][j] = B[i - 1][j - W[i - 1]] + V[i - 1];
                }
                System.out.print(B[i][j] + " ");
            }
            System.out.print("\n");
        }

        System.out.println("Max Value:\t" + B[n][M]);
        long max_capacity= B[n][M];

        System.out.println("Selected Packs: ");
        ArrayList<Long> packedItems = new ArrayList<>();
        while (n != 0) {
            if (B[n][M] != B[n - 1][M]) {
                System.out.println("\tPackage " + n + " with W = " + W[n - 1] + " and Value = " + V[n - 1]);
                packedItems.add((long) (n-1));
                M = M - W[n-1];
            }
            n--;
        }
        Collections.sort(packedItems);
        Solution solution = new Solution();
        solution.setTotal_value(max_capacity);
        solution.setPacked_items(packedItems);
        solution = solutionService.addSolution(solution);
        task.setSolution(solution);
        addTask(task);
    }
}
