package com.maersk.knapsack.rest;

import com.maersk.knapsack.jpa.Problem;
import com.maersk.knapsack.jpa.Task;
import com.maersk.knapsack.jpa.Timestamps;
import com.maersk.knapsack.model.ProblemModel;
import com.maersk.knapsack.service.TaskService;
import com.maersk.knapsack.service.ProblemService;
import com.maersk.knapsack.service.SolutionService;
import com.maersk.knapsack.service.TimestampsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;
import java.util.*;

@RestController
public class KnapsackController
{
    @Autowired TaskService taskService;
    @Autowired ProblemService problemService;
    @Autowired SolutionService solutionService;
    @Autowired TimestampsService timestampsService;

    public final static String SUBMITTED = "submitted";
    public final static String STARTED = "started";
    public final static String COMPLETED = "completed";

    /**
     * Adding problem to Knapsack
     * @param problem
     * @return
     */
    @PostMapping("/knapsack")
    public ResponseEntity addKnapsack(@RequestBody ProblemModel problem) {

        System.out.println(problem);
        String random = UUID.randomUUID().toString();
        Problem p = new Problem();
        p.setCapacity(problem.getProblem().getCapacity());
        p.setWeights(problem.getProblem().getWeights());
        p.setValues(problem.getProblem().getValues());

        Timestamps stamps = new Timestamps();
        stamps.setSubmitted(new Timestamp(System.currentTimeMillis()));
        Timestamps timestamps = timestampsService.addTimeStamps(stamps);
        com.maersk.knapsack.jpa.Task task = new Task();
        task.setProblem(problemService.addProblem(p));
        task.setStatus(SUBMITTED);
        task.setTimestamps(timestamps);
        task.setTask(random.substring(0,8));
        task = taskService.addTask(task);
        try {
            runKnapsackSolution(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(task, HttpStatus.OK);
    }

    /**
     * Get the task details
     * @param id
     * @return
     */
    @GetMapping("/knapsack/{id}")
    public ResponseEntity getKnapsackDetails(@PathVariable String id){

        System.out.println(id);
        Task task = null;
        try {
            task = taskService.getTaskByTask(id).get();
        }catch (NoSuchElementException e){
            return new ResponseEntity("Not found", HttpStatus.NOT_FOUND );
        }
        return new ResponseEntity(task, HttpStatus.OK);
    }

    /**
     * Running the Knapsack solution
     * @param task
     * @throws InterruptedException
     */
    public void runKnapsackSolution(Task task) throws InterruptedException {
        taskService.processKnapsack(task);
    }
}
