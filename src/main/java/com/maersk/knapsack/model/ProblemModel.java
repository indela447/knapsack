package com.maersk.knapsack.model;

public class ProblemModel {
    private Problem problem;

    public Problem getProblem () {
        return problem;
    }

    public void setProblem (Problem problem) {
        this.problem = problem;
    }
}
