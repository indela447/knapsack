package com.maersk.knapsack.model;

import java.util.List;

public class Problem {


    private long id;
    private int capacity;
    private List<Integer> weights;
    private List<Integer> values;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public int getCapacity () {
        return capacity;
    }

    public void setCapacity (int capacity) {
        this.capacity = capacity;
    }

    public List<Integer> getWeights () {
        return weights;
    }

    public void setWeights (List<Integer> weights) {
        this.weights = weights;
    }

    public List<Integer> getValues () {
        return values;
    }

    public void setValues (List<Integer> values) {
        this.values = values;
    }
}
