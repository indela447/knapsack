package com.maersk.knapsack;

import com.maersk.knapsack.jpa.*;
import com.maersk.knapsack.model.*;
import com.maersk.knapsack.rest.*;
import com.maersk.knapsack.service.*;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.http.*;
import org.springframework.mock.web.*;
import org.springframework.security.test.context.support.*;
import org.springframework.test.context.junit4.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = KnapsackApplication.class)
@WithMockUser
public class KnapsackControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    Task task = new Task();

    String exampleCourseJson = "{\"problem\": {\"capacity\": 60, \"weights\": [10, 20, 33], \"values\":[10, 3, 30]}}";


    @Test
    public void getTaskDetailsIfDataNotFound() throws Exception {

        Mockito.when(
            taskService.getTaskByTask(Mockito.anyString())).thenReturn(java.util.Optional.ofNullable(
            task));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
            "/knapsack/11234").accept(
            MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());

    }


}
