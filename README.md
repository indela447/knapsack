# Knapsack solution

## Setup mysql docker
```sh
docker pull mysql/mysql-server:latest
```
Then create a continer
```sh
docker run --name mysql-latest  -p 3306:3306 -p 33060:33060  -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_PASSWORD='knapsack123'   -d mysql/mysql-server:latest
```
Open mysql command shell
```sh
docker exec -it mysql-latest mysql -uroot -pknapsack123
```
Create Database
```sh
create database knapsack;
```

## Run the knapsack application

maven build in project root folder
```sh
mvn clean install
```
To generate docker image
```sh
docker build -f Dockerfile -t knapsack-solution .
```
To run docker-composer
```sh
docker-composer up
```
To shutdown docker-composer
```sh
docker-composer down
```

Run the REST endpoints

Add the problem

```sh
curl -XPOST -H 'Content-type: application/json' http://localhost:6543/knapsack -d '{"problem": {"capacity": 60, "weights": [10, 20, 33], "values":[10, 3, 30]}}'
```

Then output
```sh
{"task":"1f2ba110","status":"submitted","timestamps":{"submitted":"2021-02-22T10:29:55.258+00:00","started":null,"completed":null},"problem":{"capacity":60,"weights":[10,20,33],"values":[10,3,30]},"solution":null}
```

Then before 10 senconds run get details solution
```sh
 curl -XGET -H ''  http://localhost:6543/knapsack/1f2ba110
```

Then the output
```sh
{"task":"1f2ba110","status":"started","timestamps":{"submitted":"2021-02-22T10:29:55.000+00:00","started":"2021-02-22T10:30:05.000+00:00","completed":"null},"problem":{"capacity":60,"weights":[10,20,33],"values":[10,3,30]},"solution":null}
```
Rung again same url after 10 secons
```sh
{"task":"1f2ba110","status":"completed","timestamps":{"submitted":"2021-02-22T10:29:55.000+00:00","started":"2021-02-22T10:30:05.000+00:00","completed":"2021-02-22T10:30:15.000+00:00"},"problem":{"capacity":60,"weights":[10,20,33],"values":[10,3,30]},"solution":{"packed_items":[0,2],"total_value":40}}
```