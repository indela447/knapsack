FROM openjdk:8
ADD target/knapsack-solution.jar docker-knapsack-solution.jar
EXPOSE 6543
ENTRYPOINT ["java", "-jar", "docker-knapsack-solution.jar"]
